#! /bin/sh
export ACCOUNT_SCOPE_ID="891377046519" # Default AWS account for the scope
export MAIN_ACCOUNT=$ACCOUNT_SCOPE_ID

ACCOUNT_SCOPE_PARSED="aio"
INFRA_SCOPE_PARSED="sre"


for FILE in $(grep -r --exclude="change-data.sh" --exclude-dir=".terraform" ' ###MARK_' * | cut -d: -f1 | sort | uniq ); do
  echo "$FILE..."
  perl -pi -e "s/arglabs-\S*-terraform-states\"(.*###MARK_ACCOUNT_SCOPE_PARSED)/arglabs-$ACCOUNT_SCOPE_PARSED-terraform-states\" ###MARK_ACCOUNT_SCOPE_PARSED/g" $FILE
  perl -pi -e "s/bigbangs\/\S*\/(.*###MARK_ACCOUNT_SCOPE_PARSED)/bigbangs\/$ACCOUNT_SCOPE_PARSED\/\$1/g" $FILE
  perl -pi -e "s/bigbangs\/\S*\/(.*###MARK_INFRA_SCOPE_PARSED)/bigbangs\/$ACCOUNT_SCOPE_PARSED\/$INFRA_SCOPE_PARSED\/\$1/g" $FILE
#  perl -pi -e "s/iam::\d*:role\/(.*###MARK_MAIN_ACCOUNT)/iam::$MAIN_ACCOUNT:role\/\$1/g" $FILE
  perl -pi -e "s/\d{12}(.*###MARK_ACCOUNT_SCOPE_ID)/$ACCOUNT_SCOPE_ID\$1/g" $FILE
  perl -pi -e "s/\"\S*\/apps\/(.*).tfstate\"(.*###MARK_INFRA_SCOPE_PARSED)/\"$INFRA_SCOPE_PARSED\/apps\/\$1.tfstate\" ###MARK_INFRA_SCOPE_PARSED/g" $FILE
  perl -pi -e "s/\"\S*\/infrastructure.tfstate\"(.*###MARK_INFRA_SCOPE_PARSED)/\"$INFRA_SCOPE_PARSED\/infrastructure.tfstate\" ###MARK_INFRA_SCOPE_PARSED/g" $FILE


done

