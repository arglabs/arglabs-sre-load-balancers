data "aws_ssoadmin_instances" "this" {}
locals {
  aws_ssoadmin_instances_identity_store_id = tolist(data.aws_ssoadmin_instances.this.identity_store_ids)[0]
  aws_ssoadmin_instances_arn               = tolist(data.aws_ssoadmin_instances.this.arns)[0]
}
output "aws_ssoadmin_instances_identity_store_id" { value = local.aws_ssoadmin_instances_identity_store_id }
output "aws_ssoadmin_instances_arn" { value = local.aws_ssoadmin_instances_arn }



