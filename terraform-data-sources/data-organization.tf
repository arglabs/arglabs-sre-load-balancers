data "aws_organizations_organization" "this" {}
locals {
  aws_organizations_organization_id      = data.aws_organizations_organization.this.id
  aws_organizations_organization_root_id = data.aws_organizations_organization.this.roots[0].id
}
output "aws_organizations_organization_id" { value = local.aws_organizations_organization_id }
output "aws_organizations_organization_root_id" { value = local.aws_organizations_organization_root_id }
