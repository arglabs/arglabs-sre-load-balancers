export ACCOUNT_ID=${1:-$AWS_MAIN_ACCOUNT_ID}
export ROLE=${2:-deployer}

# Assume role and export variables
export $(\
  printf "AWS_ACCESS_KEY_ID=%s AWS_SECRET_ACCESS_KEY=%s AWS_SESSION_TOKEN=%s" $( \
    aws sts assume-role --role-arn arn:aws:iam::${ACCOUNT_ID}:role/$ROLE \
    --role-session-name BitBucket-Pipeline \
    --query "Credentials.[AccessKeyId,SecretAccessKey,SessionToken]" \
    --output text \
  ) \
)

