export AWS_WEB_IDENTITY_TOKEN_FILE="/tmp/aws-web-identity-token-file"
export AWS_ROLE_ARN="arn:aws:iam::${AWS_MAIN_ACCOUNT_ID}:role/bitbucket-openid"
echo ${BITBUCKET_STEP_OIDC_TOKEN} > ${AWS_WEB_IDENTITY_TOKEN_FILE}
