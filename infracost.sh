
export ENV=${1:-default}

infracost breakdown --path terraform/plan-$ENV.json
infracost breakdown --path terraform/plan-$ENV.json --format json --out-file infracost.json
infracost comment bitbucket --path infracost.json --tag "env:$ENV" --repo $BITBUCKET_WORKSPACE/$BITBUCKET_REPO_SLUG --commit $BITBUCKET_COMMIT --bitbucket-token $INFRACOST_BITBUCKET_TOKEN


