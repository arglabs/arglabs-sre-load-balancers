#! /bin/sh

for FILE in $(grep -r --include="*.tf" 'ref=.*\"' * | cut -d: -f1 | sort | uniq ); do
  echo "$FILE..."
  repository=$(grep -ohr 'git@.*\.git' $FILE)
  if [ -z "$1" ]; then
    TAG=$(git ls-remote --refs --tags $repository | cut -d'/' -f3 | sort -V | tail -n1)
  else
    TAG=$1 
  fi
    echo $TAG
  perl -pi -e "s/ref=[\w\d.]*/ref=$TAG/g" $FILE
done
