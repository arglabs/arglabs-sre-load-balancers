## Requirements

| Name | Version |
|------|---------|
| terraform | >= 1.2.0 |
| aws | >= 4.30.0 |

## Providers

| Name | Version |
|------|---------|
| aws | 5.57.0 |
| terraform | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| alb\_int | git::https://bitbucket.org/arglabs/arglabs-tfmod-aws-alb.git//terraform | master |

## Resources

| Name | Type |
|------|------|
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [terraform_remote_state.infrastructure](https://registry.terraform.io/providers/hashicorp/terraform/latest/docs/data-sources/remote_state) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| commit | n/a | `string` | `"Not deployed by pipeline"` | no |
| debug | n/a | `bool` | `false` | no |
| repository | n/a | `string` | `"Not deployed by pipeline"` | no |

## Outputs

| Name | Description |
|------|-------------|
| alb\_int | n/a |
| app\_name | n/a |
| aws\_provider\_data | n/a |
| debug | n/a |
| env | n/a |
| infrastructure | n/a |
| repository | output "commit" { value = local.commit } |
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| aws | n/a |
| terraform | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_organizations_organization.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/organizations_organization) | data source |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |
| [aws_ssoadmin_instances.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ssoadmin_instances) | data source |
| [terraform_remote_state.infrastructure](https://registry.terraform.io/providers/hashicorp/terraform/latest/docs/data-sources/remote_state) | data source |

## Inputs

No inputs.

## Outputs

| Name | Description |
|------|-------------|
| aws\_organizations\_organization\_id | n/a |
| aws\_organizations\_organization\_root\_id | n/a |
| aws\_ssoadmin\_instances\_arn | n/a |
| aws\_ssoadmin\_instances\_identity\_store\_id | n/a |
| infrastructure | n/a |
