#! /bin/sh
#################################################
# This is just a wrapper for running terraform  #
# operations to avoid many terraform commands   #
# on pipeline script.                           #
# But it should be kept generic and usable by   #
# every project. It must not contain project    #
# specific logic.                               #
#################################################

# Args:
export cmd=${1:-plan}
export env=${2:-default}
export dir=${3:-terraform}
export arg=$4
echo "===================================================================================="
echo "Parsed options ($0 $@): "
echo "- Command: $cmd"
echo "- Environment: $env"
echo "- Directory: $dir"

echo "===================================================================================="
# Read all vars* files with x permission
echo "Loading vars* files..."
for VARFILE in vars*.sh; do
  test -x $VARFILE && . $VARFILE && echo "- Loaded $VARFILE"
done

#################################################
# Options:
ASSUME_FINAL_ROLE="false" # If this script should assume a role after OpenID
FINAL_ROLE="deployer"     # The role
FINAL_ROLE_ACCOUNT_ID=$AWS_MAIN_ACCOUNT_ID



echo "===================================================================================="
echo "Installing needed tools... "

echo -n "- make... "    ; apk add make    >> /dev/null 2>> /dev/null; echo "OK"
echo -n "- aws-cli... " ; apk add aws-cli >> /dev/null 2>> /dev/null; echo "OK"
echo -n "- curl... "    ; apk add curl >> /dev/null 2>> /dev/null; echo "OK"

echo "===================================================================================="
echo "AWS Role information (assume another role = $ASSUME_FINAL_ROLE):"
echo "- Current role: `aws sts get-caller-identity --query \"Arn\"`"

if [ "$ASSUME_FINAL_ROLE" == "true" ]; then
  echo "Assuming role $FINAL_ROLE_ACCOUNT_ID / $FINAL_ROLE ..."
  . aws-assume-role.sh $FINAL_ROLE_ACCOUNT_ID $FINAL_ROLE || exit 1
echo "- Current role: `aws sts get-caller-identity --query \"Arn\"`"
fi


echo "===================================================================================="
echo "Variables:"
set | grep -E "TF_VAR|AWS|TOKEN" |  awk '{print "- " $1}'

echo "===================================================================================="
echo "Handing over to Makefile on $dir directory... "
cd $dir || exit 1 
case $cmd in
  init|plan|apply|destroy|plan-destroy|state-list) 
    echo "- make terraform-$cmd env=$env" ;
    make terraform-$cmd env=$env ; exit $? ;;
  output) 
    echo "- make terraform-$cmd env=$env arg=$arg" ;
    make terraform-$cmd env=$env arg=$arg ; exit $? ;;
  *) 
    echo "- make terraform-custom cmd=$cmd env=$env" ;
    make terraform-custom cmd="$cmd" env=$env ; exit $? ;;
esac
echo

