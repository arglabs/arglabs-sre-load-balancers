#######################################################
# Expected env vars (TF_VAR_)

variable "debug" {
  type    = bool
  default = false
}
output "debug" { value = var.debug ? "enabled" : "disabled" }

variable "commit" { default = "Not deployed by pipeline" }
variable "repository" { default = "Not deployed by pipeline" }

#######################################################
# Local calculations:

locals {

  env        = terraform.workspace
  commit     = var.commit
  repository = "https://bitbucket.org/${var.repository}"

}

output "env" { value = local.env }
#output "commit" { value = local.commit }
output "repository" { value = local.repository }

#######################################################
# Tags:

locals {

  # Provider Tags (to add tags even if we forget to put them on resources):
  # Must use them on provider.
  default_tags = {
    Name                 = "${local.app_name}"
    terraform            = "true"
    account_scope        = local.account_scope
    account_scope_parsed = local.account_scope_parsed
    infra_scope          = local.infra_scope
    infra_scope_parsed   = local.infra_scope_parsed
    app_name             = local.app_name
    repository           = local.repository
    env                  = local.env
  }


  # Merge with provider data if needed:
  #tags = merge(local.default_tags, local.aws_provider_data)
  tags = local.aws_provider_data

}

#output "tags" { value = local.tags }
#output "default_tags" { value = local.default_tags }

