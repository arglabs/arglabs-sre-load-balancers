module "alb_int" {
  source = "git::https://bitbucket.org/arglabs/arglabs-tfmod-aws-alb.git//terraform?ref=master"

  tags          = local.tags
  scope_parsed  = local.infra_scope_parsed
  alb_name      = "albint"
  dns_zone_id   = local.infrastructure.dns_envzone.zone_id
  internal      = true
  subnet_ids    = local.infrastructure.subnets.private_subnet_ids
  vpc_id        = local.infrastructure.vpc.vpc_id
  allowed_cidrs = ["0.0.0.0/0"]

}
output "alb_int" { value = module.alb_int }

#module "alb_ext" {
#  source = "git::https://bitbucket.org/arglabs/arglabs-tfmod-aws-alb.git//terraform?ref=master"
#
#  tags          = local.tags
#  scope_parsed  = local.infra_scope_parsed
#  alb_name      = "albext"
#  dns_zone_id   = module.aws_dns_envzone.zone_id
#  internal      = false
#  subnet_ids    = local.account_configuration.vpc.public_subnet_ids
#  vpc_id        = local.vpc_id
#  allowed_cidrs = ["0.0.0.0/0"]
#
#}
#output "alb_ext" { value = module.alb_ext }

