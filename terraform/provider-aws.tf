# Provider:
provider "aws" {
  region = local.region
  default_tags { tags = local.default_tags }

  assume_role { role_arn = "arn:aws:iam::${local.aws_account_id}:role/deployer" }
  #assume_role { role_arn = "arn:aws:iam::${local.aws_account_id}:role/OrganizationAccountAccessRole" }
}

# Provider data for tags and outputs
data "aws_caller_identity" "current" {}

locals {
  aws_provider_data = {
    aws_caller_identity_id               = data.aws_caller_identity.current.id
    aws_caller_identity_user_id          = split(":", data.aws_caller_identity.current.user_id)[0]
    aws_caller_identity_arn              = split("/", data.aws_caller_identity.current.arn)[0]
    aws_caller_identity_arn_assumed_role = split("/", data.aws_caller_identity.current.arn)[1]
    aws_caller_identity_account_id       = data.aws_caller_identity.current.account_id
  }
}

output "aws_provider_data" { value = local.aws_provider_data }



