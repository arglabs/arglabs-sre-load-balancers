#######################################################
# Expected variables:

#######################################################
# Project info: 
locals {

  app_name = "infrastructure"

}

output "app_name" { value = local.app_name }
