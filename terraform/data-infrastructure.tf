data "terraform_remote_state" "infrastructure" {
  backend   = "s3"
  workspace = local.env
  config = {
    bucket = "arglabs-aio-terraform-states" ###MARK_ACCOUNT_SCOPE_PARSED
    region = "us-east-1"
    assume_role = {
      role_arn = "arn:aws:iam::891377046519:role/deployer" ###MARK_ACCOUNT_SCOPE_ID
    }
    key = "sre/infrastructure.tfstate" ###MARK_INFRA_SCOPE_PARSED
  }
}

locals {

  infrastructure = data.terraform_remote_state.infrastructure.outputs

  # Shorts:
  region               = local.infrastructure.data_42.region
  azs                  = local.infrastructure.azs
  aws_account_id       = local.infrastructure.account_creation.aws_account_id
  account_scope        = local.infrastructure.data_42.account_scope
  account_scope_parsed = local.infrastructure.data_42.account_scope_parsed
  infra_scope          = local.infrastructure.infra_scope
  infra_scope_parsed   = local.infrastructure.infra_scope_parsed

}

output "infrastructure" { value = local.infrastructure }

