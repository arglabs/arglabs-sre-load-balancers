# Terraform:
terraform {
  required_version = ">= 1.2.0"
  required_providers { aws = { version = ">= 4.30.0" } }
  backend "s3" {
    bucket   = "arglabs-aio-terraform-states" ###MARK_ACCOUNT_SCOPE_PARSED
    region   = "us-east-1"
    acl      = "bucket-owner-full-control"
    role_arn = "arn:aws:iam::891377046519:role/deployer" ###MARK_ACCOUNT_SCOPE_ID
    key      = "sre/load-balancers.tfstate"              ###MARK_INFRA_SCOPE_PARSED
  }
}


